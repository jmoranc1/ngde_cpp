#!/usr/bin/env python3
# coding=utf-8

"""
Compile the NGDE code from Python
"""

from .ngde import ngde


__all__ = ["ngde"]
