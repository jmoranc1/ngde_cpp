/*
    NGDE_cpp: A code to simulate the agglomeration of aerosol particles
    Copyright (C) 2021  José Morán

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef AGGREGATE_H
#define AGGREGATE_H

#include "parameters.hpp"

using namespace std;

class model_phys_chem;

/********************************************************************************
* Aggregates properties, like size, mass, etc...
********************************************************************************/
class aggregate{
    friend class model_phys_chem;
private:
public:
    double Kn, Cc;                  ///< gas Knudsen number, Cunningham
    double Kn_d;                    ///< diffusive Knudsen number
    double v,m,f,Di;                ///< volumem, mass, friction, diffusion coefficient
    double r_c;                     ///< coag. radius
    double dv;                      ///< volume equivalent diameter
    double dp, rp;                  ///< primary particles diameter and radius
    double tau;                     ///< momentum relaxation time
    double c_rms,c_avg;             ///< rms and avg Maxwellian velocity
    double rg;                      ///< radius of gyration
    double df;                      ///< fractal dimension
    double lambda_p;                ///< aggregate persistant distance
    double N;                       ///< particle number concentration
    double Np;                      ///< number of monomers
    double Rs;                      ///< Smoluchowsli radius
    double s;                       ///< surface area
    /// Modifiers
    void aggregate_restart(double radius, model_phys_chem model);
    /// Getters
    void Print();
    /// Constructor
    aggregate();
    /// Destructor
    ~aggregate(){}
};


#endif // AGGREGATE_H
