/*
    NGDE_cpp: A code to simulate the agglomeration of aerosol particles
    Copyright (C) 2021  José Morán

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PARAMETERS_HPP
#define PARAMETERS_HPP

#include <iostream>
#include <math.h>
#include <fstream>
#include <stdlib.h>
#include <ctime>
#include <algorithm>
#include <map>
#include <vector>
#include <sstream>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
#include <string.h>
#include <cmath>

using namespace std;

// *************************************NOTE ON UNITS**************************************
//                            SI Units are used throught the code.
//*****************************************************************************************
/*                                     ------------                                      */
/*                                     NOMENCLATURE                                      */
/*                                     ------------                                      */
/* ______________________________________________________________________________________*/
/* Variables            	            Meaning (Units)                                  */
/* ______________________________________________________________________________________*/
/* T_g              Gas Temperature	(K)                                                  */
/* P_g              Gas Pressure	(Pa)                                                 */
/* mu_g             Gas Viscosity	(Pa*s)                                               */
/* lambda_g         Gas mean free path	(m)                                              */
/* ______________________________________________________________________________________*/
/* Constants            	            Meaning (Units)                                  */
/* ______________________________________________________________________________________*/
/* Dp               Initial primary particle diameter, geometric mean (m)                */
/* Dp_sigmaG        Initial primary particle diameter, geometric standard deviation (-)  */
/* Dpeqmass         Initial mass equivalent mean diameter (m)                            */
/* rpeqmass         Initial mass equivalent mean radius (m)                              */
/* Rho_p            Particle bulk mass density (kg/m^3)                                  */
/* vp_avg           Initial average volume of particles (m^3)                            */
/* n_t_lim          Limit residence time, a factor of tau_f (-)                          */
/* phi_p            Initial particle volume fraction (-)                                 */
/* n0               Initial particle number concentration (#/m^3)                        */
/* df0              Aggregate fractal dimension (-)                                      */
/* kf0              Aggregate fractal prefactor (-)                                      */
/* g                Factor to determine aggregates collision diameter (-)                */
/* dt_PBE           Integration time step (s)                                            */
/* t_lim            Limit residence time (s)                                             */
/* MAX              Total number of bins to siscretize the PSD (-)                       */
/* ______________________________________________________________________________________*/

//! *********** CONSTANTS *********** !
const double PI = 4.0*atan(1.0);      //!pi number 3.14
const double k_B = 1.380650524e-23;   //!Boltzmann constant

const double Ru = 8314.472;		    //!Universal ideal gas constant (J/kmol*K)
const double MM = 28.9647;		    //!Molecular weight of fluid molecules (kg/kmol)
const double T0 = 293.15;		    //!Reference temperature for Sutherland equation (K)
const double mu_0 = 18.203e-6;	    //!Reference viscosity of air for Sutherland equation (Pa*s)
const double Su = 110.4;		    //!Sutherland interpolation constant (K)

const double A1 = 1.142;		        //!Factor for Cunningham factor (1.21)
const double A2 = 0.558;		        //!Factor for Cunningham factor (0.40)
const double A3 = 0.999;		        //!Factor for Cunningham factor (1.56)

const double cc1 = 25.836;          //Gopalakrishnan params for coagulation kernels
const double cc2 = 11.211;          //Gopalakrishnan params for coagulation kernels
const double cc3 = 3.502;           //Gopalakrishnan params for coagulation kernels
const double cc4 = 7.211;           //Gopalakrishnan params for coagulation kernels

enum FrictionMethods {
    FREE_MOLECULAR,
    CORSON,
    YON,
    SORENSEN,
    INVALID_FRICTION_METHOD
};
static const std::array<const std::string, 4> FRICTION_METHODS_STR{{"free_molecular", "corson", "yon", "sorensen"}};

enum KernelMethods {
    BALLISTIC,
    DIFFUSIVE,
    HOGAN,
    MORAN2022,
    HARMONIC_MEAN,
    INVALID_KERNEL_METHOD
};
static const std::array<const std::string, 5> KERNELS_METHODS_STR{{"ballistic", "diffusive", "hogan", "moran", "harmonic_mean"}};

//! condensation model parameters
const double Rad_BAPYR = (8.087e-08/2.0)*1e-02; // c2h2: 3.3e-10 (m)
const double AMU = 1.0/(6.022e+23);
const double Mass_BAPYR = 252.31*AMU*1e-03;     // [g/mol]*[kg/g]*[mol] = [kg]; //WT(IBAPYR)*AMU;
const double Dens_BAPYR = 1240;                 // mass density, [kg/m^3]
const double CondEff = 1.00;
const double C_MW = 12.011;                     // Soot molar mass [g/mol]
const double fact_bapyr = 20.0*C_MW;
const int method_sg = 0;                        // 0: two point method (numerical diff..), 1: Rogak's method, 2: Moving sectional

/*
! *********** SOME REFERENCES *********** !
! For viscosity, the Sutherland model and values: AEROSOL MEASUREMENT 3rd edition, Kulkarni et al. (page 19)
! For gas particles mean free path: Atmospheric Chemistry and Physics 2nd edition, Seinfeld & Pandis (page 399)
! For Cunnigham correction factor parameters (for air): Rader, (1990). J of aerosol sci, 21(2), 161-168.
! For random numbers, Box and Muller method: Computational Granular Dynamics, Poschel and Schwager (2005)
! For checking collisions, Linked cell method: Computational Granular Dynamics, Poschel and Schwager (2005)
! THE PAPER DESCRIBING THIS METHOD: Prakash, A., Bapat, A. P., & Zachariah, M. R. (2003). Aerosol Sci. Tech., 37(11), 892-898.
! For the change in coagulation regime: Gopalakrishnan et al. Aerosol Science and Technology, 45:1499–1509, 2011.
*/

#define one_six 1./6.
#define one_three 1./3.

#endif // PARAMETERS_HPP
