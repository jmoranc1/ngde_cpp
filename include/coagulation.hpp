/*
    NGDE_cpp: A code to simulate the agglomeration of aerosol particles
    Copyright (C) 2021  José Morán

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef COAGULATION_H
#define COAGULATION_H

#include "model_phys_chem.hpp"
#include "aggregate.hpp"

using namespace std;

double collision_radius_TH_individ(const double df,
                                   const double np,
                                   model_phys_chem &model);
double collision_radius_TH(vector<aggregate> &agg,
                           const size_t i,
                           const size_t j,
                           model_phys_chem &model);
void calculate_kernels_diffusive(const vector<aggregate> agg,
                                 model_phys_chem &model);
void calculate_kernels_ballistic(const vector<aggregate> agg,
                                 model_phys_chem &model);
void calculate_kernels_regimes(vector<aggregate> &agg,
                               model_phys_chem &model);
void calculate_kernels_regimes_spheres(vector<aggregate> &agg,
                                       model_phys_chem &model);
void calculate_kernels_regimes_Moran(vector<aggregate> &agg,
                                     model_phys_chem &model);
void calculate_kernels_regimes_harmonic(const vector<aggregate> agg,
                                        model_phys_chem &model);
void splitting_operators_volume(const vector<aggregate> agg,
                                model_phys_chem &model);
void splitting_operators_number(const vector<aggregate> agg,
                                model_phys_chem &model);
void calculate_GainLoss_coagulaion(const vector<aggregate> agg,
                                   model_phys_chem &model);
void Integration_coagulation(vector<aggregate> &agg,
                             double &step,
                             model_phys_chem &model);

#endif // COAGULATION_H
