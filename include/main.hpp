#ifndef MAIN_H
#define MAIN_H


#include "model_phys_chem.hpp"
#include "IO.hpp"
#include "parameters.hpp"

using namespace std;

int main(int argc, char *argv[]);

#endif // MAIN_H
