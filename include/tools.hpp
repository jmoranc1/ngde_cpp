/*
    NGDE_cpp: A code to simulate the agglomeration of aerosol particles
    Copyright (C) 2021  José Morán

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TOOLS_H
#define TOOLS_H

#include "parameters.hpp"

using namespace std;

double Get_Lognormal_radius();
inline double rand_01() {return double(rand())/(RAND_MAX);};
double Get_Lognormal_pdf(const double diameter,
                         const double Dp_geo,
                         const double Dp_sigma_geo,
                         const double n0);
double sum(const double vec[]);
FrictionMethods resolve_friction_method(const std::string &input);

#endif // TOOLS_H
