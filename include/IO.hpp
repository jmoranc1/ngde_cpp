/*
    NGDE_cpp: A code to simulate the agglomeration of aerosol particles
    Copyright (C) 2021  José Morán

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef IO_H
#define IO_H

#include <inipp.h>
#include "surface_growth.hpp"
#include "coagulation.hpp"
#include "model_phys_chem.hpp"
#include "aggregate.hpp"
#include "parameters.hpp"

using namespace std;

class model_phys_chem;

/********************************************************************************
* Class for saving the data for each time iteration
********************************************************************************/
class time_data {
    friend class model_phys_chem;
private:
public:
    double t;                       ///< residence time
    double N_inf;                   ///< total particle number concentration
    double Ninf_eq;                 ///< total number concentratio (Friedlander)
    double fi;                      ///< particle volume fraction
    double dpvg;                    ///< geometric mean particles diameter (number-based)
    double dpvg_v;                  ///< geometric mean particles diameter (volume-based)
    double sigma_vg;                ///< volume eq. mean diameter (number-based)
    double sigma_vg_v;              ///< volume eq. mean diameter (volume-based)
    double kn_avg,knd_avg;          ///< average gass and diffusive Knudsen numbers
    double kij_avg;                 ///< average coagulation kernel
    double np_av;                   ///< average number of monomers per aggregate
    double dg_geo;                  ///< geometric mean gyration diameter
    double sigma_ng_dg;             ///< geometric std gyration diameter
    /// Constructor
    time_data(const model_phys_chem model);
    /// Destructor
    ~time_data(){}
};

/********************************************************************************
* Class for saving the time resolved info of the flame
********************************************************************************/
class time_flame {
    friend class model_phys_chem;
private:
public:
    double t_res;                   ///< residence time
    double Temp;                    ///< flame temperature [K]
    double sg_rate;                 ///< pyrene concentration [kg/m^3/s]
    double nucl_rate;               ///< pyrene concentration [kg/m^3/s]
    /// Constructor
    time_flame(double t_res1,
               double Temp1,
               double sg_rate1,
               double nucl_rate1);
    /// Destructor
    ~time_flame(){}
};

/********************************************************************************
* Class for saving the data for each time iteration
********************************************************************************/
class time_psd {
    friend class model_phys_chem;
private:
public:
    std::vector<double> n;                       ///< particle number concentration
    double time;
    /// Constructor
    time_psd(const vector<aggregate> agg,
             const model_phys_chem model);
    /// Destructor
    ~time_psd(){}
};

void Read_input_parameters(const std::string &input_parameters_file, model_phys_chem &model);
void IO_folder(model_phys_chem model);
void out_psd(const vector<time_psd> psd,
             const vector<aggregate> agg,
             const size_t counter,
             const size_t n_time,
             const model_phys_chem model);
void out_time_evolution(const vector<time_data> data,
                        const size_t counter,
                        const model_phys_chem model);
void load_external_coupling_data(vector<time_flame> &data_flame,
                                 model_phys_chem &model);
bool interpolate_external_coupling_data(const double t_eval,
                                        const vector<time_flame> data_flame,
                                        model_phys_chem &model);
bool update_external_coupling(const vector<time_flame> data_flame,
                              vector<aggregate> &agg,
                              model_phys_chem &model);

#endif // IO_H
