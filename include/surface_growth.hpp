/*
    NGDE_cpp: A code to simulate the agglomeration of aerosol particles
    Copyright (C) 2021  José Morán

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SURFACE_GROWTH_H
#define SURFACE_GROWTH_H

#include "model_phys_chem.hpp"
#include "aggregate.hpp"

using namespace std;

void calculate_kernels_gas_particle(const vector<aggregate> agg,
                                     model_phys_chem &model);
void calculate_surf_growth_gain(const vector<aggregate> agg,
                                model_phys_chem &model);
void Integration_surf_growth_sphere(vector<aggregate> &agg,
                                    double &step,
                                    model_phys_chem &model);
void Integration_surf_growth_agg(vector<aggregate> &agg,
                                 double &step,
                                 model_phys_chem &model);

#endif // SURFACE_GROWTH_H
