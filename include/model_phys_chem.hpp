/*
    NGDE_cpp: A code to simulate the agglomeration of aerosol particles
    Copyright (C) 2021  José Morán

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MODEL_PHYS_CHEM_H
#define MODEL_PHYS_CHEM_H

#include "tools.hpp"
#include "aggregate.hpp"

using namespace std;

/********************************************************************************
* Class to save everythong related to the model
********************************************************************************/
class model_phys_chem{
    friend class aggregate;
    friend class time_data;
    friend class time_flame;
    friend class time_psd;
private:
public:
    // Simulation properties
    bool activate_external_coupling;
    bool activate_agglomerates;
    bool activate_surface_growth;
    bool activate_nucleation;
    size_t MAX;
    size_t MAX1;
    double nucleation_rate;
    double t_lim;
    double dt_PBE;
    double t_ini;
    double Ninf;
    double Ninf_eq;
    double fi;
    double kij_avg;
    double Kmin;
    double np_avg;
    double kn_avg;
    double knd_avg;
    double t_SPD;
    double time;
    double q;
    double Ntot;
    double Vtot;

    double dg_geo;
    double sigma_ng_dg;
    
    std::vector<std::vector<std::vector<double>>> X;
    std::vector<std::vector<double>> K;
    std::vector<double> sum1;
    std::vector<double> sum2;
    std::vector<double> K_gas;
    std::vector<double> sum_cond;
    std::vector<double> I_sg;
    std::vector<double> u_sg;
    // Gas properties
    double T_g;
    double P_g;
    double mu_g;
    double lambda_g;
    double sg_rate;
    // Particles
    double Dp;
    double Dp_sigmaG;
    double vp_avg;
    double Rho_p;
    double n0;
    double phi_p;
    double df0;
    double kf0;
    double g;
    double Dpeqmass;
    double rpeqmass;
    double dpvg;        // number-based
    double dpvg_v;      // volume-based
    double sigma_vg;    // number-based
    double sigma_vg_v;  // volume-based
    double dpav;
    double tau_f;
    // IO
    string output_directory;
    string input_coupling_directory;
    FrictionMethods friction_method;
public:
    /// Setters
    /// Getters
    void SUB_show_parameters(const vector<aggregate> agg,
                             const double t_lim);
    /// Modifiers
    void Update_temperature(const double new_temperature);
    double friction_coeff_YON(const double npp,
                              const double rpeqmass,
                              const double dfe);
    double friction_coeff_SORENSEN(const double npp);
    double friction_coeff_CORSON(const double npp, const double rpeqmass);
    double friction_coeff_FREE_MOLECULAR(const double npp, const double rpeqmass);
    double Friction_coeff_Methods(double df, double np, double rp);
    void GEO_mean_std(const vector<aggregate> agg);
    void Update_aggregates_external(vector<aggregate> &agg);
    void Aggregate_restart(const double radius, const double n_adim0, aggregate &agg);
    void Restart_aggregates(const double radius, const double n_adim0, vector<aggregate> &agg);
    void Initialize_aggregates(vector<aggregate> &agg);
    void Update_aggregates_surf_growth(aggregate &agg);
    /// Constructor
    void Initialize_vectors();
    model_phys_chem();
    /// Destructor
    ~model_phys_chem(){}
};

#endif // MODEL_PHYS_CHEM_H
