/*
    NGDE_cpp: A code to simulate the agglomeration of aerosol particles
    Copyright (C) 2021  José Morán

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "model_phys_chem.hpp"

model_phys_chem::model_phys_chem():
    activate_external_coupling(false),
    activate_agglomerates(true),
    activate_surface_growth(false),
    activate_nucleation(false),
    friction_method(FrictionMethods::CORSON),
    MAX(41),
    MAX1(42),
    nucleation_rate(0.0),
    t_lim(1.0),
    dt_PBE(1e-05),
    t_ini(0.0),
    Ninf(0.0),
    Ninf_eq(0.0),
    fi(0.0),
    kij_avg(0.0),
    Kmin(0.0),
    np_avg(1.0),
    kn_avg(0.0),
    knd_avg(0.0),
    t_SPD(0.0),
    time(0.0),
    q(0.0),
    T_g(300),
    P_g(101300),
    mu_g(18.203e-6),
    lambda_g(66e-09),
    sg_rate(0.0),
    Dp(1.0),
    Dp_sigmaG(1.0),
    vp_avg(1),
    Rho_p(1e+03),
    n0(1e+17),
    phi_p(1e-06),
    df0(1.78),
    kf0(1.30),
    g(1.0),
    Dpeqmass(1.0),
    rpeqmass(0.5),
    dpvg(1.0),
    dpvg_v(1.0),
    sigma_vg(1.0),
    sigma_vg_v(1.0),
    dpav(1.0),
    tau_f(0.0),
    output_directory("output_dir"),
    input_coupling_directory("input_dir") {
}

void model_phys_chem::Initialize_vectors(){
    X.resize(MAX1);
    K.resize(MAX1);
    K_gas.resize(MAX1);
    sum1.resize(MAX1);
    sum2.resize(MAX1);
    sum_cond.resize(MAX1);
    I_sg.resize(MAX1);
    u_sg.resize(MAX1);
    for(size_t i=0; i<MAX1; i++) {
        X[i].resize(MAX1);
        K[i].resize(MAX1);
        sum1[i] = 0.0;
        sum2[i] = 0.0;
        K_gas[i] = 0.0;
        sum_cond[i] = 0.0;
        I_sg[i] = 0.0;
        u_sg[i] = 0.0;
        for(size_t j=0; j<MAX1; j++) {
            X[i][j].resize(MAX1);
            K[i][j] = 0;
            for(size_t k=0; k<MAX1; k++) {
                X[i][j][k] = 0;
            }
        }
    }
}

/********************************************************************************
* Update gas temperature and dependent properties
********************************************************************************/
void model_phys_chem::Update_temperature(const double new_temperature){
    T_g = new_temperature;
    mu_g = mu_0*((T0+Su)/(T_g+Su))*std::pow(T_g/T0,3./2.);
    lambda_g = 2.*mu_g/(P_g*std::pow(8.*MM/(PI*Ru*T_g),0.5));
}

/********************************************************************************
* Print main physical properties on the screen
********************************************************************************/
void model_phys_chem::SUB_show_parameters(const vector<aggregate> agg,
                                          const double t_lim){
    //! ********** simulation ********** !
    std::cout <<"SIMULATION" << std::endl;
    std::cout <<" nodes                       "   << MAX  << std::endl;
    std::cout <<" n0 (#/m^3)                  "   << n0  << std::endl;
    std::cout <<" phi_p (ppm)                 "   << phi_p*std::pow(10.,6)   << std::endl;
    std::cout <<" residence time (ms)         "   << t_lim*std::pow(10.,3)   << std::endl;
    std::cout <<" "                              << std::endl;
    //! ********** fluid ********** !
    std::cout <<"FLUID PROPERTIES" << std::endl;
    std::cout <<" Temperature (K)             "	<< T_g  << std::endl;
    std::cout <<" Pressure (Pa)               "	<< P_g  << std::endl;
    std::cout <<" Viscosity (kg/m*s)          "	<< mu_g  << std::endl;
    std::cout <<" Fluid mfp (nm)              "	<< lambda_g*std::pow(10.,9)  << std::endl;
    std::cout <<" Maxwell avg vel (m/s)       "	<< std::sqrt(8.*Ru*T_g/(PI*MM))  << std::endl;
    std::cout <<" " 							<< std::endl;
    //! ********** particles ********** !
    std::cout <<"PARTICLES PROPERTIES"  		<< std::endl;
    std::cout <<" GEO_mean(dp) (nm)           "	<< Dp*(1e+09) << std::endl;
    std::cout <<" GEO_std(dp)                 "	<< Dp_sigmaG << std::endl;
    std::cout <<" vp_avg (m^3)                "	<< vp_avg << std::endl;
    std::cout <<" Mass density (kg/m^3)       "	<< Rho_p  << std::endl;
    std::cout <<" size range max(dp)/dp0      "	<< agg[MAX+1].dv/agg[1].dv << std::endl;
    std::cout <<" "                              << endl;
    std::cout <<" i - Dv (nm), Kn, Cc, lambda_p (nm),  --  f, Np, N "	<< std::endl;
    for (size_t i = 0; i < MAX + 1; i++)
    {
        std::cout << i << " - " << agg[i].dv * std::pow(10., 9.0) << " " << agg[i].Kn << " " << agg[i].Cc << " " << agg[i].lambda_p * std::pow(10., 9) << "  --  " << agg[i].f << " " << agg[i].Np << " " << agg[i].N << std::endl;
    };
}

/********************************************************************************
* Obtain the friction cofficient of aggregates
********************************************************************************/
double model_phys_chem::friction_coeff_CORSON(const double npp, const double rpeqmass){
    double Kn = lambda_g/rpeqmass;
    double cc = 1.+Kn*(A1+A2*std::exp(-A3/Kn));
    double temp = (1.+1.612*Kn)*std::pow(std::pow(0.852*std::pow(npp,0.535)+0.148,-1)+1.612*Kn*std::pow(0.843*std::pow(npp,0.939)+0.157,-1),-1);

    return (6.0*PI*mu_g*rpeqmass/cc)*temp;
}

double model_phys_chem::friction_coeff_SORENSEN(const double npp){
    double Rm = rpeqmass*pow(npp, 0.46);    //Free molecular regime
    double Kn = lambda_g/Rm;
    double cc = 1.+Kn*(A1+A2*std::exp(-A3/Kn));

    return 6.0*PI*mu_g*Rm/cc;
}

double model_phys_chem::friction_coeff_YON(const double npp,
                                           const double rpeqmass,
                                           const double dfe){
    double Kn = lambda_g/rpeqmass;
    double gamma_ = 1.378*(0.5+0.5*std::erf((Kn+4.454)/10.628));
    double cc = 1.+Kn*(A1+A2*std::exp(-A3/Kn));

    return (6.0*PI*mu_g*rpeqmass/cc)*std::pow(npp,gamma_/dfe);
}

double model_phys_chem::friction_coeff_FREE_MOLECULAR(const double npp, const double rpeqmass){
    double alpha = 1.08;
    double k_alpha = 1.0;
    double A_pp = PI*std::pow(rpeqmass,2);
    double A_proj = A_pp * std::pow(npp/k_alpha,1/alpha);
    double Rm = std::pow(A_proj/PI, 0.5);    //Free molecular regime
    double Kn = lambda_g/Rm;
    double cc = 1.+Kn*(A1+A2*std::exp(-A3/Kn));

    return 6.0*PI*mu_g*Rm/cc;
}

/********************************************************************************
* Determine friction coefficient according to selected method
********************************************************************************/
double model_phys_chem::Friction_coeff_Methods(double df, double np, double rp){
    double friction(0);
    if (friction_method == FrictionMethods::CORSON) {
        friction = friction_coeff_CORSON(np, rp);
    } else if (friction_method == FrictionMethods::SORENSEN) {
        friction = friction_coeff_SORENSEN(np);
    } else if (friction_method == FrictionMethods::YON){
        friction = friction_coeff_YON(np, rp, df);
    } else if (friction_method == FrictionMethods::FREE_MOLECULAR){
        friction = friction_coeff_FREE_MOLECULAR(np, rp);
    }
    return friction;
}

/********************************************************************************
* Calculate GEO-mean and GEO-std
********************************************************************************/
void model_phys_chem::GEO_mean_std(const vector<aggregate> agg) {
    double Ntot_sq(0.0);

    double Slog_dv(0.0);
    double Slog_dv_v(0.0);
    
    double Slog_sv(0.0);
    double Slog_sv_v(0.0);

    double Slog_dg(0.0);
    double Slog_ng_dg(0.0);

    np_avg = 0.0;
    knd_avg = 0.0;
    kn_avg = 0.0;
    Ntot = 0.0;
    Vtot = 0.0;


    for(size_t i=1; i<MAX1; i++){
        np_avg += agg[i].N*agg[i].Np;
        Ntot += agg[i].N;             //Total number of particles (include monomers).
        Vtot += agg[i].N*agg[i].v;    //Total volume of particles.
        knd_avg += agg[i].Kn_d * agg[i].N;
        kn_avg += agg[i].Kn * agg[i].N;
        Slog_dv += agg[i].N*std::log(agg[i].dv);
        Slog_dv_v += agg[i].N*agg[i].v*std::log(agg[i].dv);

        Slog_dg += agg[i].N*std::log(2 * agg[i].rg);
    }

    fi = Vtot;                          // Volume fraction
    Ninf = Ntot;                        // Total number concentration

    np_avg = np_avg/Ntot;               // Average number of primary particles (number average)
    double Vav = Vtot/Ntot;                    // Average volume of particles (number average)
    dpav = std::pow((6.0*Vav/PI),one_three);   //Volume based mean diameter of particles
    dpvg = std::exp(Slog_dv/Ntot);    // number-based geometric mean
    dpvg_v = std::exp(Slog_dv_v/Vtot);  // volume-based geometric mean

    dg_geo = std::exp(Slog_dg/Ntot);    // number-based geometric mean gyration diameter

    knd_avg = knd_avg/Ntot;
    kn_avg = kn_avg/Ntot;

    for(size_t i=1; i<MAX1; i++){
        Slog_sv += agg[i].N*std::pow(std::log(agg[i].dv/dpvg),2); // number based
        Slog_sv_v += agg[i].N * agg[i].v * std::pow(std::log(agg[i].dv/dpvg_v),2); // number based

        Slog_ng_dg += agg[i].N*std::pow(std::log(2*agg[i].rg/dg_geo),2); // number based
    }
    sigma_vg = std::exp(std::sqrt(Slog_sv/Ntot));
    sigma_vg_v = std::exp(std::sqrt(Slog_sv_v/Vtot));

    sigma_ng_dg = std::exp(std::sqrt(Slog_ng_dg/Ntot));

    for(size_t i=1; i<MAX1; i++){
        for(size_t j=1; j<MAX1; j++){
            Ntot_sq += agg[i].N*agg[j].N;
            kij_avg += K[i][j]*agg[i].N*agg[j].N;
        }
    }
    kij_avg = kij_avg/Ntot_sq;
}

/********************************************************************************
* Update aggregates by external coupling
********************************************************************************/
void model_phys_chem::Update_aggregates_external(vector<aggregate> &agg){
    for (size_t i=1; i<agg.size(); i++){
        // Update aggregates/spheres separately
        if (activate_agglomerates) {
            agg[i].f = Friction_coeff_Methods(agg[i].df, agg[i].Np, agg[i].rp);
        } else {
            agg[i].f = Friction_coeff_Methods(agg[i].df, agg[i].Np, 0.5*agg[i].dv);
        }

        // Fluid-particle interaction
        agg[i].c_rms = std::sqrt(3.0*k_B*T_g/agg[i].m);
        agg[i].c_avg = std::sqrt(8.*k_B*T_g/(PI*agg[i].m));
        agg[i].Di = k_B*T_g/agg[i].f;
        agg[i].tau = agg[i].m/agg[i].f;
        agg[i].Kn = lambda_g/agg[i].Rs;

        // Particle-particle interaction
        agg[i].lambda_p = std::sqrt(18.*agg[i].Di*agg[i].tau);
        agg[i].Kn_d = agg[i].lambda_p/agg[i].Rs;

        //agg[i].Print();
    }
}

/********************************************************************************
* Update aggregates by external coupling
********************************************************************************/
void model_phys_chem::Restart_aggregates(const double radius,
                                         const double n_adim0,
                                         vector<aggregate> &agg){
 for (size_t i=1; i<agg.size(); i++){
     Aggregate_restart(radius, n_adim0, agg[i]);
 }
}

/********************************************************************************
* Update aggregates by external coupling
********************************************************************************/
void model_phys_chem::Aggregate_restart(const double radius,
                                        const double n_adim0,
                                        aggregate &agg){
    // Primary particle properties
    agg.dp = Dp;
    agg.rp = 0.5 * Dp;

    // Aggregates properties
    agg.v = (4.0*PI/3.0)*(radius*radius*radius);
    agg.s = (4.0*PI)*std::pow(agg.rp,2)*std::exp(2*std::pow(std::log(Dp_sigmaG),2))*agg.Np;
    agg.m = Rho_p * agg.v;
    agg.dv = std::pow((6*agg.v/PI),one_three);
    agg.r_c = g*agg.rg;

    if (activate_agglomerates) {
        agg.Np = n_adim0;
        agg.rg = (Dp/2.0)*std::pow(agg.Np/kf0,1/df0);
        agg.df = df0;
        agg.f = Friction_coeff_Methods(agg.df, agg.Np, agg.rp);
    } else {
        agg.Np = 1.0;
        agg.rg = (agg.dv/2.0)*std::pow(3/5,1/2);
        agg.Rs = 0.5 * agg.dv;
        agg.df = 3;
        agg.f = Friction_coeff_Methods(agg.df, agg.Np, 0.5*agg.dv);
    }

    // Fluid-particle interaction
    agg.Kn = lambda_g/agg.Rs;
    agg.Cc = 1.+agg.Kn*(A1+A2*std::exp(-A3/agg.Kn));
    agg.c_rms = std::sqrt(3.0*k_B*T_g/agg.m);
    agg.c_avg = std::sqrt(8.*k_B*T_g/(PI*agg.m));
    agg.Di = k_B*T_g/agg.f;
    agg.tau = agg.m/agg.f;

    // Particle-particle interaction
    agg.N = 0.0;
    agg.lambda_p = std::sqrt(18.*agg.Di*agg.tau);
    agg.Kn_d = agg.lambda_p/agg.Rs;
}

void model_phys_chem::Initialize_aggregates(vector<aggregate> &agg){
    Ninf_eq = n0;
    agg.push_back(aggregate());
    Aggregate_restart(Dp*0.5, 1, agg.back());

    if (activate_agglomerates){
        for(size_t i=1; i < MAX1; i++) {
            agg.push_back(aggregate());
            double n_adim = 1.0;
            double v_temp = vp_avg*n_adim;
            double dv_temp = std::pow((6.0*v_temp/PI),one_three);
            Aggregate_restart(0.5*dv_temp, n_adim, agg.back());
        };
        // Calculating volumes of larger nodes using a geometric spacing factor of q
        for(size_t i=1; i < MAX1; i++) {
            double n_adim = std::pow(q,(i-1));
            double v_temp = vp_avg*n_adim;
            double dv_temp = std::pow((6.0*v_temp/PI),one_three);
            Aggregate_restart(0.5*dv_temp, n_adim, agg[i]);
        };
        // Normalizing psd in terms of the number concentrations
        agg[1].N = n0;
        for(size_t i=2; i < MAX1; i++) {
            agg[i].N = 0.0;
        }
    } else{
        double minDp = Dp/std::pow(Dp_sigmaG,2);
        double minVp = (PI/6.0)*std::pow(minDp,3);
        for(size_t i=1; i < MAX1; i++) {
            double radius = minDp*0.5;
            agg.push_back(aggregate());
            Aggregate_restart(radius, 1, agg.back());
        };
        double sum_n = 0; // sum the number concentration of sections
        // Calculating volumes of larger nodes using a geometric spacing factor of q
        for(size_t i=1; i < MAX1; i++) {
            double v_adim = std::pow(q,(i-1));
            double v_temp = minVp*v_adim;
            double dp_temp = std::pow((6.0*v_temp/PI),one_three);
            Aggregate_restart(0.5*dp_temp, 1, agg[i]);
            agg[i].N = Get_Lognormal_pdf(dp_temp, Dp, Dp_sigmaG, n0);
            sum_n += agg[i].N;
        };
        // Normalizing psd in terms of the number concentrations
        if (Dp_sigmaG > 0.9999999999999 && Dp_sigmaG < 1.00000001) {
            agg[1].N = n0;
            for(size_t i=2; i < MAX1; i++) {
                agg[i].N = 0.0;
            }
        } else {
            for(size_t i=1; i < MAX1; i++) {
                agg[i].N = agg[i].N*n0/sum_n;
            }
        }
    }
}

/********************************************************************************
* Update aggregates by surface growth
********************************************************************************/
void model_phys_chem::Update_aggregates_surf_growth(aggregate &agg){

    // Primary particle properties
    double v_pp = (PI/6.0) * std::pow(agg.dp,3);
    double s_pp = PI * std::pow(agg.dp,2);

    // Aggregates properties
    agg.v = agg.Np * v_pp;
    agg.s = agg.Np * s_pp;
    agg.m = Rho_p*agg.v;
    agg.dv = std::pow((6.0*agg.v/PI),one_three);

    if (activate_agglomerates) {
        agg.rg = (Dp/2.0)*std::pow(agg.Np/kf0,1/df0);
        agg.f = Friction_coeff_Methods(agg.df, agg.Np, agg.rp);
    } else {
        agg.Rs = 0.5 * agg.dv;
        agg.rg = (agg.dv/2.0)*std::pow(3/5,1/2);
        agg.f = Friction_coeff_Methods(agg.df, agg.Np, 0.5*agg.dv);
    }
    agg.r_c = g*agg.rg;

    // Fluid-particle interaction
    agg.Kn = lambda_g/agg.Rs;
    agg.Cc = 1.+agg.Kn*(A1+A2*std::exp(-A3/agg.Kn));
    agg.c_rms = std::sqrt(3.0*k_B*T_g/agg.m);
    agg.c_avg = std::sqrt(8.*k_B*T_g/(PI*agg.m));
    agg.Di = k_B*T_g/agg.f;
    agg.tau = agg.m/agg.f;

    // Particle-particle interaction
    agg.lambda_p = std::sqrt(18.*agg.Di*agg.tau);
    agg.Kn_d = agg.lambda_p/agg.Rs;
}
