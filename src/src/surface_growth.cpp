/*
    NGDE_cpp: A code to simulate the agglomeration of aerosol particles
    Copyright (C) 2021  José Morán

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "surface_growth.hpp"

/********************************************************************************
* Calculate the collision kernels GAS - PARTICULA
********************************************************************************/
void calculate_kernels_gas_particle(const vector<aggregate> agg,
                                    model_phys_chem &model) {
    // MODIFIED: Gopalakrishnan
    double a_ij;                // relative coagulation radius
    double m_ij;                // reduced mass
    double f_ij;                // reduced friction coefficient
    double Kn_d2;               // relative diffusive Knudsen number
    double H_gop;               // dimensionless coag. kernel

    for(size_t i=1; i<model.MAX1; i++){
        a_ij = agg[i].Rs*Rad_BAPYR/(Rad_BAPYR+agg[i].Rs);

        m_ij = agg[i].m*Mass_BAPYR/(agg[i].m + Mass_BAPYR);
        f_ij = agg[i].f;

        Kn_d2 = sqrt(k_B*model.T_g*m_ij)/(f_ij*a_ij);

        H_gop = (4.0*PI*pow(Kn_d2,2)+cc1*pow(Kn_d2,3)+sqrt(8.0*PI)*cc2*pow(Kn_d2,4))/(1.0+cc3*Kn_d2+cc4*pow(Kn_d2,2)+cc2*pow(Kn_d2,3));

        //[m^3/s] =[kg/s] * [m^3]         [-]/[kg]
        model.K_gas[i] = f_ij * pow(a_ij,3) * H_gop/m_ij;
    }
    model.K_gas[0] = 0;
    model.K_gas[model.MAX+1] = 0;
}

void calculate_surf_growth_gain(const vector<aggregate> agg,
                                model_phys_chem &model) {
    double sum_tot(0);
    for(size_t i=0; i<model.MAX1; i++){
        model.sum_cond[i] = 0.0;
        model.I_sg[i] = 0.0;
    }
/*
    for(size_t i=1; i<model.MAX1; i++){
        //[m^3/s] = [m^3/s]  * [-]   * [mol/m^3]          *  [kg/mol]       /[kg/m^3]
        model.I_sg[i] = model.K_gas[i] * CondEff * (model.bapyr_conc*1e+06) * (Mass_BAPYR/AMU)/model.Rho_p; //bapyr_conc*1e+06 Dens_BAPYR
    }
*/
    model.sum_cond[1] = -model.I_sg[1]*agg[1].N/(agg[2].v-agg[1].v);
    sum_tot += model.sum_cond[1];
    for(size_t i=2;i <=model.MAX-1; i++){
        model.sum_cond[i] = model.I_sg[i-1]*agg[i-1].N/(agg[i].v-agg[i-1].v) - model.I_sg[i]*agg[i].N/(agg[i+1].v-agg[i].v);
        sum_tot += model.sum_cond[i];
    }
    model.sum_cond[model.MAX] = model.I_sg[model.MAX-1]*agg[model.MAX-1].N/(agg[model.MAX].v-agg[model.MAX-1].v);

    sum_tot += model.sum_cond[model.MAX];
    std::cout << " sum_tot: " << sum_tot << std::endl;

    model.sum_cond[0] = 0;
    model.sum_cond[model.MAX+1] = 0;
}

/********************************************************************************
* Surface growth time integration
********************************************************************************/
void Integration_surf_growth_sphere(vector<aggregate> &agg,
                                    double &step,
                                    model_phys_chem &model) {
    model.fi = 0.0;                   // Particles volume fraction
    model.Ninf = 0.0;                 // Total number conc. of particles, which is zero initially.
    for(size_t i=1; i<model.MAX1; i++){
        agg[i].N = agg[i].N + step*(model.sum_cond[i]);

        if (agg[i].N < 0) {
            std::cout <<" i= " << i<<" MAX " << model.MAX <<" error, N[i] cannot be negative!! " << agg[i].N << std::endl; //exit (EXIT_FAILURE);
            agg[i].N = 0.0;
        };

        model.Ninf = model.Ninf + agg[i].N;     //N_infinity as we get by solving the coagulation part of GDE.
    };

    if (model.Ninf < 0) {
        std::cout <<" Error, Ninf cannot be negative!! " << std::endl;
        exit (EXIT_FAILURE);
    };

    step = model.dt_PBE/(model.Kmin*model.Ninf);   //Adaptive timestep for integration, based on characteristic coagulation time.
}

/********************************************************************************
* Surface growth time integration - AGGREGATES
********************************************************************************/
void Integration_surf_growth_agg(vector<aggregate> &agg,
                                 double &step,
                                 model_phys_chem &model) {
    for(size_t i=1; i<model.MAX1; i++){
        model.u_sg[i] = model.I_sg[i] * 2/model.Rho_p/agg[i].s;
        agg[i].rp = agg[i].rp + step * model.u_sg[i];
        agg[i].dp = 2 * agg[i].rp;
        model.Update_aggregates_surf_growth(agg[i]);
    };
}
