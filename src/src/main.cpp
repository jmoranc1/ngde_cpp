/*
    NGDE_cpp: A code to simulate the agglomeration of aerosol particles
    Copyright (C) 2021  José Morán

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "main.hpp"

/********************************************************************************
* Main program
********************************************************************************/
int main(int argc, char *argv[]) {
    std::cout << std::endl;
    std::cout << "NGDE_cpp  Copyright (C) 2021 Jose Moran" << std::endl;
    std::cout << "  This program comes with ABSOLUTELY NO WARRANTY" << std::endl;
    std::cout << "  This is free software, and you are welcome to redistribute it" << std::endl;
    std::cout << "  under certain conditions" << std::endl;
    std::cout << std::endl;
    if (argc <= 1) {
        std::cout << "Missing argument: input parameter file." << std::endl;
        return 1;
    }
    model_phys_chem model;                  // class to save the information of the model
    // Read input parameters
    Read_input_parameters(argv[1],model);
    // make folder for output data
    IO_folder(model);

    std::vector<aggregate> agg;              // object particle: agg (to save the infgormation about aggregate)
    std::vector<time_data> data;             // object to save statistical data as a function of time
    std::vector<time_psd> psd;               // object to save the data of PSD
    std::vector<time_flame> data_flame;      // object to save the time resolved flame information

    // Load the information from external coupling file
    if (model.activate_external_coupling){
        std::cout << "Load external coupling data" << std::endl;
        load_external_coupling_data(data_flame,model);
    }

    // Initialize aggregates
    model.Initialize_aggregates(agg);

    // Calculation of size splitting operators (volume-, or number-based)
    if (model.activate_agglomerates){
        std::cout << "Spliting operators - number" << std::endl;
        splitting_operators_number(agg, model);
    } else{
        std::cout << "Spliting operators - volume" << std::endl;
        splitting_operators_volume(agg, model);
    }

    // Initializing time.
    model.time = model.t_ini;

   // update info according to external coupling
    if (model.activate_external_coupling){
        update_external_coupling(data_flame, agg, model);
    }
    // Calculate coagulation kernels
    if (model.activate_agglomerates){
        calculate_kernels_regimes(agg, model);
    } else{
        std::cout << "Kernels determined for different regimes (spheres)" << std::endl;
        //calculate_kernels_regimes_spheres(agg, model);
        calculate_kernels_regimes_Moran(agg, model);
    }
    model.Update_aggregates_external(agg);

    double time_step = model.dt_PBE/(model.Kmin*model.n0);

    // CALCULATE AND SAVE INITIAL DATA
    model.GEO_mean_std(agg);
    model.tau_f = 2.0/(model.kij_avg*model.n0);
    data.push_back(time_data(model));
    psd.push_back(time_psd(agg,model));

    // Just show the parameters of the simulation
    model.SUB_show_parameters(agg,model.t_lim);

    // Variable to count the time iterations
    size_t t_iter(0);
    bool finished(false);
    while(!finished) {
        // Update info according to external coupling
        if (model.activate_external_coupling){
            finished = update_external_coupling(data_flame, agg, model);
        }

        // Integrate nucleation (Assuming they always nucleate in node 1!)
        if (model.activate_nucleation){
            agg[1].N += time_step*model.nucleation_rate/agg[1].m;
        }

        // Calculating the gain and loss terms due to coagulation and integrate
        calculate_GainLoss_coagulaion(agg, model);
        Integration_coagulation(agg, time_step, model);

        // Calculate the gain/loss term due to surface growth
        if (model.activate_surface_growth && !model.activate_agglomerates){
            calculate_surf_growth_gain(agg, model);
            Integration_surf_growth_sphere(agg, time_step, model);
        } else if(model.activate_surface_growth && model.activate_agglomerates){
            calculate_surf_growth_gain(agg, model);
            Integration_surf_growth_agg(agg, time_step, model);
        }

        model.time += time_step;

        // Calculate the geometric mean diameter and sigma_gv (dpvg & sigma_vg, respectively)
        model.GEO_mean_std(agg);
        data.push_back(time_data(model));
        psd.push_back(time_psd(agg,model));
        /*
        if(t_iter%10 == 0)
        {
            data.push_back(time_data(model));
            psd.push_back(time_psd(agg,model));
        } */
        t_iter += 1;

        // Print partial results as a function of time
        std::cout.precision(3);
        std::cout << std::scientific << t_iter << " time t (ms) = " << model.time*1e+03
                  << "  N_inf " << model.Ninf << " (" << model.Ninf_eq << ")  ---  phi "
                  << model.fi  <<" step = "<< time_step <<" sigma_vg = " << model.sigma_vg
                  << " np_avg = " << model.np_avg << std::endl;

        // if we detect a problem in psd(MAX) we finish the simulation
        double dif = std::sqrt(agg[1].v*std::pow(model.q,model.MAX)*agg[1].v*std::pow(model.q,model.MAX-1))-
                std::sqrt(agg[1].v*std::pow(model.q,model.MAX-1)*agg[1].v*std::pow(model.q,model.MAX-2));
        double c = psd[t_iter].n[model.MAX]/dif;

        if(c>1e-08){
            std::cout << " STOPPED - ERROR! - Problem with node MAX " << std::endl;
            out_time_evolution(data,0,model);
            //out_psd(psd, agg, 0, t_iter,model);
            exit (EXIT_FAILURE);
        };

        if ((model.time > model.t_lim) && !model.activate_external_coupling){
            std::cout << " Simulation ended by limit in residence time. " << std::endl;
            finished = true;
        }
    };

    std::cout << "Finished succesfully, output data saved on "
              << model.output_directory << std::endl;
    out_time_evolution(data,0,model);
    out_psd(psd, agg, 0, t_iter,model);

    return 0;
}
