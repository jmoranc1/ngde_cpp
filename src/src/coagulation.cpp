/*
    NGDE_cpp: A code to simulate the agglomeration of aerosol particles
    Copyright (C) 2021  José Morán

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "coagulation.hpp"

/********************************************************************************
* Thajudeen collision radius
********************************************************************************/
double collision_radius_TH_individ(const double df,
                                   const double Np,
                                   model_phys_chem &model) {
    double alpha1_i = 0.253*std::pow(df,2)-1.209*df+1.433;
    double alpha2_i = -0.218*std::pow(df,2)+0.964*df-0.180;

    double phiR_i = 1.0/(alpha1_i*std::log(Np)+alpha2_i);

    double Rs_i_ai = (model.Dp*0.5)*phiR_i*std::pow(Np/model.kf0,1.0/df);

    return Rs_i_ai;
}

double collision_radius_TH(vector<aggregate> &agg,
                           const size_t i,
                           const size_t j,
                           model_phys_chem &model) {

    agg[i].Rs = collision_radius_TH_individ(agg[i].df, agg[i].Np,model);
    agg[j].Rs = collision_radius_TH_individ(agg[j].df, agg[j].Np,model);

    double Rs_i_ai = agg[i].Rs/(model.Dp*0.5);
    double Rs_j_aj = agg[j].Rs/(model.Dp*0.5);

    double tmp = std::pow(Rs_i_ai+Rs_j_aj,0.8806+0.3497*(agg[i].Np+agg[j].Np)/(agg[i].Np*agg[i].df+agg[j].Np*agg[j].df));

    double Rs_ij = (model.Dp*0.5)*(1.203-0.4315*(agg[i].Np+agg[j].Np)/(agg[i].Np*agg[i].df+agg[j].Np*agg[j].df))*tmp;

    return Rs_ij;
}

/********************************************************************************
* Calculate the collision kernels K[i][j]
********************************************************************************/
// 0. Supossing always in the diffusive regime
void calculate_kernels_diffusive(const vector<aggregate> agg,
                                 model_phys_chem &model) {
    double temp1,temp2;
    model.Kmin=1;    //setting an arbitrary value for Kmin to start with.

    for(size_t i=1; i<model.MAX1; i++) {
        for(size_t j=1; j<model.MAX1; j++) {
            // Based on eq. (8.13) of Friedlander 2000.
            temp1 = 1.0/std::pow(agg[i].v,1.0/agg[i].df) + 1.0/std::pow(agg[j].v,1.0/agg[j].df);
            temp2 = std::pow(agg[i].v,1.0/agg[i].df) + std::pow(agg[j].v,1.0/agg[j].df);

            model.K[i][j]= (2.0*k_B*model.T_g/(3.0*model.mu_g))*temp1*temp2;

            if (model.K[i][j] < model.Kmin) {
                model.Kmin = model.K[i][j]; //Calculating the smallest collision frequency function to decide the characteristic coagulation time.
            }
        };
    };
}

// 1. Supossing always in the ballistic regime
void calculate_kernels_ballistic(const vector<aggregate> agg,
                                 model_phys_chem &model) {
    double temp1,temp2;
    model.Kmin = 1;    //setting an arbitrary value for Kmin to start with.
    double lambda;

    for(size_t i=1; i<model.MAX1; i++) {
        for(size_t j=1; j<model.MAX1; j++) {
            //temp1 = 1.0/agg[i].v + 1.0/agg[j].v;
            //temp2 = pow(agg[i].v,one_three) + pow(agg[j].v,one_three);
            //K[i][j]= pow(3.0/(4.0*PI),one_six)*pow((6.0*k_B*T_g/Rho_p),0.5)*pow(temp1,0.5)*pow(temp2,2.0);

            // Based on eq. (8.11) of Friedlander 2000.
            lambda = 2.0/agg[i].df-1.0/2.0;
            temp1 = 1.0/agg[i].v + 1.0/agg[j].v;
            temp2 = std::pow(agg[i].v,1.0/agg[i].df) + std::pow(agg[j].v,1.0/agg[j].df);
            model.K[i][j]= std::pow((6.0*k_B*model.T_g/model.Rho_p),0.5)*std::pow(3.0/(4.0*PI),lambda)*std::pow(model.Dp*0.5,2.0-6.0/agg[i].df)*std::pow(temp1,0.5)*std::pow(temp2,2.0);
            if (model.K[i][j] < model.Kmin) {
                model.Kmin = model.K[i][j]; //Calculating the smallest collision frequency function to decide the characteristic coagulation time.
            }
        };
    };
}

// 2. Considering a change in flow regime
void calculate_kernels_regimes(vector<aggregate> &agg,
                               model_phys_chem &model) {
    // MODIFIED: Gopalakrishnan
    double a_ij;                // relative coagulation radius
    double m_ij;                // reduced mass
    double f_ij;                // reduced friction coefficient
    double Kn_d2;               // relative diffusive Knudsen number
    double H_gop;               // dimensionless coag. kernel
    //double m,rg;                // total mass and radius of gyration
    model.Kmin = 1;                     //setting an arbitrary value for Kmin to start with.

    for(size_t i=1; i<model.MAX1; i++) {
        for(size_t j=1; j<model.MAX1; j++) {
            a_ij = collision_radius_TH(agg, i, j, model);

            m_ij = agg[i].m*agg[j].m/(agg[i].m + agg[j].m);
            f_ij = agg[i].f*agg[j].f/(agg[i].f + agg[j].f);

            Kn_d2 = std::sqrt(k_B*model.T_g*m_ij)/(f_ij*a_ij);

            H_gop = (4.0*PI*std::pow(Kn_d2,2)+cc1*std::pow(Kn_d2,3)+
                     std::sqrt(8.0*PI)*cc2*std::pow(Kn_d2,4))/(1.0+cc3*Kn_d2+
                                                               cc4*std::pow(Kn_d2,2)+cc2*std::pow(Kn_d2,3));

            model.K[i][j] = f_ij*std::pow(a_ij,3)*H_gop/m_ij;
            if (model.K[i][j] < model.Kmin) {
                model.Kmin = model.K[i][j]; //Calculating the smallest collision frequency function to decide the characteristic coagulation time.
            }
        };
    };
}

void calculate_kernels_regimes_spheres(vector<aggregate> &agg,
                                       model_phys_chem &model) {
    // MODIFIED: Gopalakrishnan
    double a_ij;                // relative coagulation radius
    double m_ij;                // reduced mass
    double f_ij;                // reduced friction coefficient
    double Kn_d2;               // relative diffusive Knudsen number
    double H_gop;               // interpolation formula parameters
    model.Kmin = 1;             //setting an arbitrary value for Kmin to start with.

    for(size_t i=1; i<model.MAX1; i++) {
        for(size_t j=1; j<model.MAX1; j++) {
            a_ij = 0.5 * (agg[i].dv + agg[j].dv);

            m_ij = agg[i].m*agg[j].m/(agg[i].m + agg[j].m);
            f_ij = agg[i].f*agg[j].f/(agg[i].f + agg[j].f);

            Kn_d2 = std::sqrt(k_B*model.T_g*m_ij)/(f_ij*a_ij);

            H_gop = (4.0*PI*std::pow(Kn_d2,2)+cc1*std::pow(Kn_d2,3)+
                     std::sqrt(8.0*PI)*cc2*std::pow(Kn_d2,4))/(1.0+cc3*Kn_d2+
                                                               cc4*std::pow(Kn_d2,2)+cc2*std::pow(Kn_d2,3));

            model.K[i][j] = f_ij*std::pow(a_ij,3)*H_gop/m_ij;
            if (model.K[i][j] < model.Kmin) {
                model.Kmin = model.K[i][j]; //Calculating the smallest collision frequency function to decide the characteristic coagulation time.
            }
        };
    };
}

// 2. Considering a change in flow regime
void calculate_kernels_regimes_harmonic(const vector<aggregate> agg,
                                        model_phys_chem &model) {
    double temp1,temp2;
    double lambda;
    double K_ball, K_diff;
    model.Kmin = 1;                     //setting an arbitrary value for Kmin to start with.

    for(size_t i=1; i<model.MAX1; i++) {
        for(size_t j=1; j<model.MAX1; j++) {
            // BALLISTIC COAGULATION KERNEL
            temp1 = 1.0/std::pow(agg[i].v,1.0/agg[i].df) + 1.0/std::pow(agg[j].v,1.0/agg[j].df);
            temp2 = std::pow(agg[i].v,1.0/agg[i].df) + std::pow(agg[j].v,1.0/agg[j].df);
            K_ball = (2.0*k_B*model.T_g/(3.0*model.mu_g))*temp1*temp2;

            // DIFFUSIVE COAGULATION KERNEL
            lambda = 2.0/agg[i].df-1.0/2.0;
            temp1 = 1.0/agg[i].v + 1.0/agg[j].v;
            temp2 = std::pow(agg[i].v,1.0/agg[i].df) + std::pow(agg[j].v,1.0/agg[j].df);
            K_diff = std::pow((6.0*k_B*model.T_g/model.Rho_p),0.5)*std::pow(3.0/(4.0*PI),lambda)*std::pow(model.Dp*0.5,2.0-6.0/agg[i].df)*std::pow(temp1,0.5)*std::pow(temp2,2.0);

            model.K[i][j] = K_ball*K_diff/(K_ball+K_diff);
            if (model.K[i][j] < model.Kmin) {
                model.Kmin = model.K[i][j]; //Calculating the smallest collision frequency function to decide the characteristic coagulation time.
            }
        };
    };
}
void calculate_kernels_regimes_Moran(vector<aggregate> &agg,
                                     model_phys_chem &model) {
    double a_ij;                // relative coagulation radius
    double m_ij;                // reduced mass
    double f_ij;                // reduced friction coefficient
    double Kn_d2;               // relative diffusive Knudsen number
    double D_ij;                // sum of diffusion coefficients
    double u_ij;                // relative velocity
    double f;                   // correction function transition
    double K_diff;              // diffusive kernel
    //double m,rg;              // total mass and radius of gyration
    model.Kmin = 1;                     //setting an arbitrary value for Kmin to start with.

    for(size_t i=1; i<model.MAX1; i++) {
        for(size_t j=1; j<model.MAX1; j++) {
            a_ij = 0.5 * (agg[i].dv + agg[j].dv);

            m_ij = agg[i].m*agg[j].m/(agg[i].m + agg[j].m);
            f_ij = agg[i].f*agg[j].f/(agg[i].f + agg[j].f);

            D_ij = k_B*model.T_g/f_ij;
            u_ij = std::sqrt(8 * k_B*model.T_g/PI/m_ij);

            K_diff = 4*PI*a_ij*D_ij;

            Kn_d2 = 8 * std::sqrt(2) * D_ij/PI/u_ij/a_ij;

            f = 1/std::sqrt(1+std::pow(PI * Kn_d2,2)/8);

            model.K[i][j] = K_diff * f;
            if (model.K[i][j] < model.Kmin) {
                model.Kmin = model.K[i][j]; //Calculating the smallest collision frequency function to decide the characteristic coagulation time.
            }
        };
    };
}

/********************************************************************************
* Calculation of size splitting operators
********************************************************************************/
void splitting_operators_volume(const vector<aggregate> agg,
                                model_phys_chem &model) {
    double v_sum(0.);
    for(size_t k=1; k<model.MAX1; k++) {
        for(size_t i=1; i<model.MAX1; i++) {
            for(size_t j=1; j<model.MAX1; j++) {
                v_sum = agg[i].v + agg[j].v;
                //Conditions in parentheses check if the combined volume of colliding particles is between k and k+1.
                if (v_sum > agg[model.MAX].v) {
                    model.X[i][j][model.MAX]= v_sum/agg[model.MAX].v;

                } else if (agg[k].v < v_sum && v_sum < agg[k+1].v) {
                    model.X[i][j][k] = (agg[k+1].v - v_sum)/(agg[k+1].v - agg[k].v);
                } else if(agg[k-1].v < v_sum && v_sum < agg[k].v) {
                    model.X[i][j][k] = (v_sum - agg[k-1].v)/(agg[k].v-agg[k-1].v);
                } else {
                    model.X[i][j][k] = 0.0;
                }
            }
        }
    }
}

void splitting_operators_number(const vector<aggregate> agg,
                                model_phys_chem &model) {
    for(size_t k=1; k<model.MAX1; k++) {
        for(size_t i=1; i<model.MAX1; i++) {
            for(size_t j=1; j<model.MAX1; j++) {
                //Conditions in parentheses check if the combined Np of colliding particles is between k and k+1.
                if (agg[i].Np+agg[j].Np >= agg[model.MAX].Np) {
                    model.X[i][j][model.MAX]= (agg[i].Np+agg[j].Np)/agg[model.MAX].Np;

                } else if (agg[k].Np <= (agg[i].Np+agg[j].Np) && (agg[i].Np+agg[j].Np)<agg[k+1].Np) {
                    model.X[i][j][k]=(agg[k+1].Np-agg[i].Np-agg[j].Np)/(agg[k+1].Np-agg[k].Np);
                } else if(agg[k-1].Np <= (agg[i].Np+agg[j].Np) && (agg[i].Np+agg[j].Np)<agg[k].Np) {
                    model.X[i][j][k]=(agg[i].Np+agg[j].Np-agg[k-1].Np)/(agg[k].Np-agg[k-1].Np);
                } else {
                    model.X[i][j][k]=0.0;
                }
            }
        }
    }
}

/********************************************************************************
* Calculate the gain and loss terms of the PBE due to coagulation
********************************************************************************/
void calculate_GainLoss_coagulaion(const vector<aggregate> agg,
                                   model_phys_chem &model) {
    for(size_t k=1; k<model.MAX1; k++) {
        model.sum1[k] = 0.0;      //Addition term when i and j collide to form a k sized particle.
        model.sum2[k] = 0.0;      //Subtraction term when k collides with any other particle.
        for(size_t i=1; i<model.MAX1; i++) {
            model.sum2[k] = model.sum2[k] + model.K[k][i]*agg[k].N*agg[i].N;
            for(size_t j=1;j<=k;j++) {
                model.sum1[k] = model.sum1[k] + 0.5 * model.X[i][j][k]*model.K[i][j]*agg[i].N*agg[j].N; // [m^3/s]*[1/m^3]*[1/m^3] = [1/m^3/s]
            }
        }
    }
}

/********************************************************************************
* Coagulation time integration
********************************************************************************/
void Integration_coagulation(vector<aggregate> &agg,
                             double &step,
                             model_phys_chem &model) {
    model.fi = 0.0;                   // Particles volume fraction
    model.Ninf = 0.0;                 // Total number of particles, which is zero initially.
    for(size_t i=1; i<model.MAX1; i++){
        agg[i].N += step*(model.sum1[i] - model.sum2[i]);

        if (agg[i].N < 0) {
            std::cout <<" i= " << i<<" / " << model.MAX <<" error, N[i] cannot be negative!! " << agg[i].N << std::endl; //exit (EXIT_FAILURE);
            agg[i].N = 0.0;
        };

        model.Ninf += agg[i].N;     //N_infinity as we get by solving the coagulation part of GDE.
    }

    if (model.Ninf < 0) {
        std::cout <<" Error, Ninf cannot be negative!! " << std::endl;
        exit (EXIT_FAILURE);
    };

    step = model.dt_PBE/(model.Kmin*model.Ninf);   //Adaptive timestep for integration, based on characteristic coagulation time.


    for(size_t i=1; i<model.MAX1; i++) {
        model.fi += agg[i].N*agg[i].v;
    };
    //N_infinity according to equation 7.77 in Friedlander.
    model.Ninf_eq = model.Ninf_eq - step*((6.67/2)*std::pow((3/(4*PI)),one_six)*std::pow(6*k_B*model.T_g/model.Rho_p,0.5)*std::pow(model.fi,one_six)*std::pow(model.Ninf_eq,(11.0/6.0)));
}

/********************************************************************************
* RECALCULATE NODES - avoid peak of frequency in agg[MAX].N
********************************************************************************/
void recalculate_nodes(const double q, size_t &nodes_it,
                       vector<aggregate> &agg,
                       const model_phys_chem model){
    double radius;
    double v_adim,v_temp,dp_temp;
    // delete first element in the array
    agg.erase (agg.begin());

    // add a new element to the array
    nodes_it  = nodes_it+1;
    radius = model.Dp*0.5; //Get_Lognormal_radius();
    //agg.push_back(aggregate(radius,model));

    // update new element in the array
    v_adim = std::pow(q,(nodes_it-1));
    v_temp = model.vp_avg*v_adim;
    dp_temp = std::pow((6*v_temp/PI),one_three);
    //agg[nodes_it].aggregate_restart(0.5*dp_temp,v_adim,model);
}
