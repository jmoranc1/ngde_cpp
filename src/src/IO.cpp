/*
    NGDE_cpp: A code to simulate the agglomeration of aerosol particles
    Copyright (C) 2021  José Morán

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <boost/filesystem.hpp>
#include "IO.hpp"

void Read_input_parameters(const std::string &input_parameters_file, model_phys_chem &model){
    std::cout << "Read parameters from: " << input_parameters_file << std::endl;
    bool temp_external_coupling(false);
    bool temp_surface_growth(false);
    bool temp_nucleation(false);
    bool temp_agglomerates(true);
    double temp_temperature;
    std::string default_str;
    // read the config file
    inipp::Ini<char> ini;
    std::ifstream is(input_parameters_file);
    ini.parse(is);
    ini.interpolate();
    // Gas properties
    inipp::extract(ini.sections["Gas_properties"]["pressure"], model.P_g);
    inipp::extract(ini.sections["Gas_properties"]["temperature"], temp_temperature);
    model.Update_temperature(temp_temperature);
    // Simulation options
    inipp::extract(ini.sections["Simulation"]["total_time"], model.t_lim);
    inipp::extract(ini.sections["Simulation"]["number_nodes"], model.MAX);
    inipp::extract(ini.sections["Simulation"]["time_step"], model.dt_PBE);
    inipp::extract(ini.sections["Simulation"]["initial_time"], model.t_ini);
    inipp::extract(ini.sections["Simulation"]["activate_external_coupling"], temp_external_coupling);
    inipp::extract(ini.sections["Simulation"]["activate_agglomerates"], temp_agglomerates);
    if (temp_agglomerates){
        model.activate_agglomerates = true;
        std::cout << " - Agglomeration is activated." << std::endl;
    } else {
        model.activate_agglomerates = false;
        std::cout << " - Agglomeration is not activated." << std::endl;
    }
    inipp::extract(ini.sections["Simulation"]["activate_surface_growth"], temp_surface_growth);
    if (temp_surface_growth){
        model.activate_surface_growth = true;
        std::cout << " - Surface growth is activted." << std::endl;
    } else {
        std::cout << " - Surface growth is not activated." << std::endl;
    }
    model.MAX1 = model.MAX+2;
    model.q = std::pow(10.0,(12.0/(model.MAX-2))); //Calculation of geometric spacing factor which depends on the number of nodes.
    inipp::extract(ini.sections["Simulation"]["activate_nucleation"], temp_nucleation);
    if (temp_nucleation){
        model.activate_nucleation = true;
        inipp::extract(ini.sections["Simulation"]["nucleation_rate"], model.nucleation_rate);
        std::cout << " - Nucleation is activted." << std::endl;
    } else {
        std::cout << " - Nucleation is not activated." << std::endl;
    }
    inipp::extract(ini.sections["Simulation"]["method_friction"], default_str);
    if (default_str != "") {
        model.friction_method = resolve_friction_method(default_str);
        std::cout << " - Method friction coefficient: " << default_str << std::endl;
    } else {
        std::cout << " - Method friction coefficient: UNKNOWN (using default)" << std::endl;
    }
    // Particles
    inipp::extract(ini.sections["Particles"]["initial_number_concentration"], model.n0);
    
    // Primary particle diameter
    double Dp_in;
    inipp::extract(ini.sections["Particles"]["initial_geometric_mean_d"], Dp_in);
    model.Dp = Dp_in;
    inipp::extract(ini.sections["Particles"]["initial_geometric_std_d"], model.Dp_sigmaG);
    inipp::extract(ini.sections["Particles"]["mass_bulk_density"], model.Rho_p);
    inipp::extract(ini.sections["Particles"]["fractal_dimension"], model.df0);
    inipp::extract(ini.sections["Particles"]["fractal_prefactor"], model.kf0);
    model.Dpeqmass = model.Dp*std::exp(1.5*std::log(model.Dp_sigmaG)*std::log(model.Dp_sigmaG));
    model.rpeqmass = model.Dpeqmass*0.5;
    model.vp_avg = (PI/6)*std::pow(model.Dp,3)*std::exp(4.5 * std::pow(std::log(model.Dp_sigmaG),2));
    model.phi_p = model.vp_avg*model.n0;
    model.g = std::sqrt((model.df0+2.0)/model.df0);
    // The following time-lag for SPSD is valid for spheres in FM regime
    model.t_SPD = 5/(std::pow(3.0/(4.0*PI),one_six)*std::pow((6.0*k_B*model.T_g/model.Rho_p),0.5)*std::pow(model.vp_avg,one_six)*model.n0);
    // IO
    inipp::extract(ini.sections["IO"]["output_directory"], model.output_directory);
    std::cout << " - output_directory: " << model.output_directory << std::endl;
    if (temp_external_coupling){
        model.activate_external_coupling = true;
        inipp::extract(ini.sections["IO"]["input_external_coupling"], model.input_coupling_directory);
        std::cout << " - Read external coupling file from: " << model.input_coupling_directory << std::endl;
    } else {
        std::cout << " - External coupling not activated" << std::endl;
    }
    std::cout << "Successfully read parameters" << std::endl;
    // Initialize vectors of the model
    model.Initialize_vectors();
}

void IO_folder(model_phys_chem model){
    // create the output folder
    boost::filesystem::path p(model.output_directory);
    if (boost::filesystem::exists(p)){
        std::cout << " Output directory already exists. It has been removed. " << std::endl;
        boost::filesystem::remove_all(p);
    }
    boost::filesystem::create_directory(p);
    std::cout << "Output directory " << p << std::endl;
}

/********************************************************************************
* write particle size distribution to a file
********************************************************************************/
void out_psd(const vector<time_psd> psd,
             const vector<aggregate> agg,
             const size_t counter,
             const size_t n_time,
             const model_phys_chem model){
    FILE * pFile;
    char buffer[50];
    int precision = 10;
    std::string fpath;
    fpath+=model.output_directory + "/PBE_PSD_";
    gcvt(model.MAX+1, precision, buffer);
    fpath+=buffer;
    fpath+="_";
    gcvt(counter, precision, buffer);
    fpath+=buffer;
    fpath+=".dat";
    char* pch2 = (char*)malloc( sizeof( char ) *(fpath.length() +1) );
    std::string::traits_type::copy( pch2, fpath.c_str(), fpath.length() +1 );
    pFile = fopen (pch2,"w");
    if (pFile == NULL) perror("Error opening file");
    else{
        fprintf(pFile,"%le ",0.0);
        fprintf(pFile,"%le ",0.0);
        fprintf(pFile,"%le ",0.0);
        for (size_t i=0;i<=n_time;i++)
        {
            fprintf(pFile,"%le ",psd[i].time);
        }
        fprintf(pFile,"\n");
        for (size_t j=0;j<=model.MAX+1;j++)
        {
            fprintf(pFile,"%le ", agg[j].dv);
            fprintf(pFile,"%le ", 2*agg[j].rg);
            fprintf(pFile,"%le ", agg[j].Np);
            for (size_t i=0;i<=n_time;i++)
            {
                fprintf(pFile,"%le ",psd[i].n[j]);
            }
            fprintf(pFile,"\n");
        };
        fclose (pFile);
    }
    return;
}

/********************************************************************************
* write time-resolved data to a text file
********************************************************************************/
void out_time_evolution(const vector<time_data> data,
                        const size_t counter,
                        const model_phys_chem model){
    size_t n_data=data.size();

    FILE * pFile;
    char buffer[50];
    int precision = 10;
    std::string fpath;
    fpath+=model.output_directory + "/PBE_";
    gcvt(model.MAX+1, precision, buffer);
    fpath+=buffer;
    fpath+="_";
    gcvt(counter, precision, buffer);
    fpath+=buffer;
    fpath+="time_evolution.dat";
    char* pch2 = (char*)malloc( sizeof( char ) *(fpath.length() +1) );
    std::string::traits_type::copy( pch2, fpath.c_str(), fpath.length() +1 );
    pFile = fopen (pch2,"w");
    if (pFile == NULL) perror("Error opening file");
    else{
        fprintf(pFile,"\"t\" \"Num_agg\" \"Ninf_eq\" \"vol_frac\" ");
        fprintf(pFile,"\"Dv_GEOn\" \"Dv_GEOv\" \"Dv_GSTDn\" \"Dv_GSTDv\" ");
        fprintf(pFile,"\"kn_g\" \"kn_d\" \"k_ij\" \"np_avg\" \"dg_geo\" \"sigma_ng_dg\"\n");
        for (size_t i=0;i<n_data;i++)
        {
            fprintf(pFile,"%le %le %le %le %le %le %le %le %le %le %le %le %le %le \n",
                    data[i].t,
                    data[i].N_inf,
                    data[i].Ninf_eq,
                    data[i].fi,
                    data[i].dpvg,
                    data[i].dpvg_v,
                    data[i].sigma_vg,
                    data[i].sigma_vg_v,
                    data[i].kn_avg,
                    data[i].knd_avg,
                    data[i].kij_avg,
                    data[i].np_av,
                    data[i].dg_geo,
                    data[i].sigma_ng_dg);
        }
        fclose (pFile);
    }
    return;
}

/********************************************************************************
* import the time resolved information of the flame
********************************************************************************/
void load_external_coupling_data(vector<time_flame> &data_flame,
                                 model_phys_chem &model){
    std::ifstream ext_FILE;
    ext_FILE.open (model.input_coupling_directory, std::ifstream::in);

    //WE ASUME: External DATA CONTAINS 7 COLUMNS AS: [HAB,t_res,T,fv,dp_av,c2h2,bapyr,J_nucl]
    array<double,8> current_number;

    std::cout <<"LOAD THE EXTERNAL COUPLING INFORMATION (as a function of time)" << std::endl;
    while (ext_FILE >>
           current_number[0] >>
           current_number[1] >>
           current_number[2] >>
           current_number[3])
    {
        std::cout.precision(3);
        std::cout << std::scientific
                  << current_number[0] << " "
                  << current_number[1] << " "
                  << current_number[2] << " "
                  << current_number[3] << std::endl;
        // becareful with the units (SI) in the imput file!
        data_flame.push_back(time_flame(current_number[0],
                                        current_number[1],
                                        current_number[2],
                                        current_number[3]));
    }
    // Close the file.
    ext_FILE.close();
    // Update the limit residence time
    model.t_lim = data_flame.back().t_res;
}

time_data::time_data(const model_phys_chem model) {
    t = model.time;
    N_inf = model.Ninf;
    Ninf_eq = model.Ninf_eq;
    fi = model.fi;
    dpvg = model.dpvg;
    dpvg_v = model.dpvg_v;
    sigma_vg = model.sigma_vg;
    sigma_vg_v = model.sigma_vg_v;
    kn_avg = model.kn_avg;
    knd_avg = model.knd_avg;
    kij_avg = model.kij_avg;
    np_av = model.np_avg;

    dg_geo = model.dg_geo;
    sigma_ng_dg = model.sigma_ng_dg;
}



time_flame::time_flame(double t_res1,
                       double Temp1,
                       double sg_rate1,
                       double nucl_rate1) {
    t_res = t_res1;
    Temp = Temp1;
    sg_rate = sg_rate1;
    nucl_rate = nucl_rate1;
}


time_psd::time_psd(const vector<aggregate> agg,
                   const model_phys_chem model) {
    n.resize(model.MAX1);
    time = model.time;
    for(size_t i=0; i<model.MAX1; i++) {
        n[i] = agg[i].N; //*fii/pow(Ninf,2)); //agg[i].N/Ninf; //(agg[i].N*fii/pow(Ninf,2)); ///v_avg;
    }
}



bool interpolate_external_coupling_data(const double t_eval,
                                        const vector<time_flame> data_flame,
                                        model_phys_chem &model) {
    if (t_eval < data_flame.front().t_res) {
        std::cout << " STOPPED - ERROR! - Current time is lower than the minimum in external coupling. " << std::endl;
        exit (EXIT_FAILURE);
    }
    if (t_eval > data_flame.back().t_res){
        std::cout << " Simulation ended by external coupling. " << std::endl;
        return true;
    }
    double vr,vl;

    size_t i(0);
    while(data_flame[i].t_res < t_eval){
        i += 1;
    }

    // Times
    double tr = data_flame[i].t_res;
    double tl = data_flame[i-1].t_res;

    // temperature
    vr = data_flame[i].Temp;
    vl = data_flame[i-1].Temp;
    model.T_g = vl + ((vr-vl)/(tr-tl))*(t_eval-tl);

    //SG rate
    vr = data_flame[i].sg_rate;
    vl = data_flame[i-1].sg_rate;
    model.sg_rate = vl + ((vr-vl)/(tr-tl))*(t_eval-tl); // [kg/m^3/s]

    // Nucleation flux
    vr = data_flame[i].nucl_rate;
    vl = data_flame[i-1].nucl_rate;
    model.nucleation_rate = vl + ((vr-vl)/(tr-tl))*(t_eval-tl);

    return false;
}

/********************************************************************************
* Update all the info according to the flame information
********************************************************************************/
bool update_external_coupling(const vector<time_flame> data_flame,
                              vector<aggregate> &agg,
                              model_phys_chem &model) {
    bool finished_external_coupling(false);
    double t_eval = model.time;
    // calculate the new temperature
    finished_external_coupling = interpolate_external_coupling_data(t_eval, data_flame,model);

    if (finished_external_coupling){
        return true;
    }

    // update temperature dependent properties
    model.Update_temperature(model.T_g);

    // update aggregates info
    model.Update_aggregates_external(agg);

    // update coagulation kernels
    calculate_kernels_regimes(agg, model);

    // update kernels particle-gas
    if (model.activate_surface_growth){
        calculate_kernels_gas_particle(agg, model);
    }
    return finished_external_coupling;
}
