/*
    NGDE_cpp: A code to simulate the agglomeration of aerosol particles
    Copyright (C) 2021  José Morán

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "aggregate.hpp"

aggregate::aggregate():
    Kn(0.0),
    Cc(0.0),
    Kn_d(0.0),
    v(0.0),
    m(0.0),
    f(0.0),
    Di(0.0),
    r_c(0.0),
    dv(0.0),
    dp(0.0),
    rp(0.0),
    tau(0.0),
    c_rms(0.0),
    c_avg(0.0),
    rg(0.0),
    df(0.0),
    lambda_p(0.0),
    N(0.0),
    Np(1),
    Rs(0.0),
    s(0.0) {

}

/********************************************************************************
* Show the properties of an aggregate
********************************************************************************/
void aggregate::Print(){
    std::cout << "Print aggregate " << "\n" <<
                 " Np           "  << Np << "\n" <<
                 " c_rms        "  << c_rms << "\n" <<
                 " c_avg        "  << c_avg << "\n" <<
                 " f            "  << f << "\n" <<
                 " Di           "  << Di << "\n" <<
                 " tau          "  << tau << "\n" <<
                 " Kn_g         "  << Kn << "\n" <<
                 " Kn_d         "  << Kn_d << "\n" <<
                 " Rs           "  << Rs << "\n" <<
                 " lambda_p     "  << lambda_p << std::endl;
}
