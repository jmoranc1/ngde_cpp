/*
    NGDE_cpp: A code to simulate the agglomeration of aerosol particles
    Copyright (C) 2021  José Morán

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "tools.hpp"

/********************************************************************************
* Lognormal PSD - random sampling
********************************************************************************/
double Get_Lognormal_pdf(const double diameter,
                         const double Dp_geo,
                         const double Dp_sigma_geo,
                         const double n0){
    if (Dp_sigma_geo > 0.9999999999999){
        double temp = n0/(std::sqrt(2.0*PI)*diameter*std::log(Dp_sigma_geo));
        double temp2 = std::exp(-0.5*std::pow((std::log(diameter/Dp_geo)/std::log(Dp_sigma_geo)),2));
        return temp*temp2;
    } else {
        std::cout <<" Error, Dp_sigmaG cannot be lower than 1" << std::endl;
        exit (EXIT_FAILURE);
    };
}

/********************************************************************************
* Sum elements in an array
********************************************************************************/
double sum(const double vec[], size_t vec_size){
    double sum_(0.0);
    for (size_t i=1; i<=vec_size; i++){
        sum_ += vec[i];
    }
    return sum_;
}

/********************************************************************************
* Friction methods
********************************************************************************/
FrictionMethods resolve_friction_method(const std::string &input) {
    auto itr = std::find(FRICTION_METHODS_STR.begin(),
                         FRICTION_METHODS_STR.end(),
                         input);
    if (itr != FRICTION_METHODS_STR.end()) {
        return FrictionMethods(std::distance(FRICTION_METHODS_STR.begin(), itr));
    }
    return INVALID_FRICTION_METHOD;
}
