#!/usr/bin/env python3
# coding=utf-8
"""Compile and install pyNGDE"""
import sys
from subprocess import run, CalledProcessError

from setuptools import setup as setup
from setuptools import Extension as Extension

setup()

# noinspection PyPep8
import numpy as np

ngde = Extension(name='pyNGDE.ngde_wrapper',
                     sources=['../pyNGDE/ngde_wrapper.pyx','src/main.cpp'],
                     include_dirs=[np.get_include(),'include','src'],
                     extra_compile_args=["-fopenmp", "-O3"],
                     extra_link_args=["-fopenmp"],
                     language="c++")
# noinspection PyUnusedName
ngde.cython_c_in_temp = True

try:
    # Build the cython extension
    # --------------------------
    setup(ext_modules=[ngde])
except BaseException as e:
    print("\n################### ERROR ##################################", file=sys.stderr)
    print("Unable to compile PCA", file=sys.stderr)
    print(e, file=sys.stderr)
    print("################### ERROR ##################################\n", file=sys.stderr)
