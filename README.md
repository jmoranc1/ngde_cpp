[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

# NGDE++: Overview

Population Balance code to simulate Aerosol Dynamics, based on the General Dynamics or Population Balance Equation (PBE). An improved C++ version adapted to simulate the agglomeration of aerosol particles with the possibility of coupling with external CFD simulations is provided.

The principles used to solve the PBE are thorougly explained in,
*  My presentation ["Population Balance Equation (PBE) modeling of soot agglomeration"](https://gitlab.com/jmoranc1/ngde_cpp/-/blob/master/documents/JMoran_PBE_soot.pdf)
* Prakash, A., Bapat, A. P., & Zachariah, M. R. (2003). A simple numerical algorithm and software for solution of nucleation, surface growth, and coagulation problems. Aerosol Science & Technology, 37(11), 892-898.

## Developer

My name is [José Morán](htttps://josecmoranc.github.io), currently Post-doc associate at University of Minnesota, USA.

## Installation

Download the current version of the code

    git clone git@gitlab.com:jmoranc1/ngde_cpp.git
    
Compile the code. Open a terminal and run the following line:

    sh install.sh

## Update

Go to the "NGDE_cpp" folder and type in the terminal:

    git pull
    
Then, recompile the new version (you may first delete all the files within the "build" folder).

## Examples

Sample scripts are provided in the folder: [examples](https://gitlab.com/jmoranc1/ngde_cpp/-/tree/master/examples)


## Python

Using Python for post-processing is not mandatory but highly recomended. To use it, you should use the virtual environment automatically created during compilation.

## License

NGDE_cpp is an open-source package, this mean you can use or modify it under the terms and conditions of the GPL-v3 licence. You should have received a copy along with this package, if not please refer to [https://www.gnu.org/licenses/](https://www.gnu.org/licenses/).

## Cite us

If you use the codes provided in this repository you are kindly asked to cite our work:

Morán, J. (2022). On the Link between the Langevin Equation and the Coagulation Kernels of Suspended Nanoparticles. Fractal and Fractional, 6(9), 529. https://doi.org/10.3390/fractalfract6090529

